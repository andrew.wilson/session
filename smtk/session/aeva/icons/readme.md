Icons were originally produced in Inkscape, available for all platforms.
- Open the original icon .svg for modifications, save.
- Choose 'Save As...', 'Optimized .svg', choose the '\_opt.svg' version of the file.
- In the options, 'SVG Output' tab, check 'Remove metadata'
- The 'all_primitives_feature.svg' icon wasn't showing the arrows in Qt. Before saving the '\_opt' file, select each of the lines with double arrows and choose 'Path .. Stroke to path'. Paths render correctly in Qt.
