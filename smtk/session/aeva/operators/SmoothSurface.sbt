<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the aeva "smooth surface" Operation -->
<SMTK_AttributeResource Version="3">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <AttDef Type="smooth surface" Label="Smooth Surface" BaseType="operation">

      <BriefDescription>Smooth a surface using a windowed sinc function interpolation kernel</BriefDescription>
      <DetailedDescription>
        Smooth a surface using a windowed sinc function interpolation kernel

        For each vertex v, a topological and geometric analysis is
        performed to determine which vertices are connected to v, and
        which cells are connected to v. Then, a connectivity array is
        constructed for each vertex. (The connectivity array is a list of lists
        of vertices that directly attach to each vertex.) Next, an iteration
        phase begins over all vertices. For each vertex v, the coordinates of v
        are modified using a windowed sinc function interpolation kernel.
        Taubin describes this methodology in the IBM tech report:

        RC-20404 (#90237, dated 3/12/96) "Optimal Surface Smoothing as Filter Design"
        G. Taubin, T. Zhang and G. Golub.
      </DetailedDescription>
      <AssociationsDef Name="source" NumberOfRequiredValues="1">
        <BriefDescription>The input surface to smooth.</BriefDescription>
        <Accepts><Resource Name="smtk::session::aeva::Resource" Filter="face"/></Accepts>
      </AssociationsDef>

      <ItemDefinitions>

        <Int Name="number of iterations" Label="Number of Iterations">
          <BriefDescription>Number of iterations (i.e., the degree of
          the polynomial approximating the windowed sinc function).
          </BriefDescription>
          <DefaultValue>20</DefaultValue>
          <RangeInfo>
            <Min Inclusive="false">0</Min>
          </RangeInfo>
        </Int>

        <Double Name="passband" Label="PassBand">
          <BriefDescription>The passband value for the windowed sinc filter.</BriefDescription>
          <DefaultValue>0.1</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Max Inclusive="true">2.</Max>
          </RangeInfo>
        </Double>

        <Void Name="normalize coordinates" Label="Normalize Coordinates" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>
            Turn on/off coordinate normalization.
          </BriefDescription>
          <DetailedDescription>
            Turn on/off coordinate normalization.

            The positions can be translated and scaled such that they fit
            within a [-1, 1] prior to the smoothing computation. The
            default is off.  The numerical stability of the solution
            can be improved by turning normalization on. If
            normalization is on, the coordinates will be rescaled to
            the original coordinate system after smoothing has completed.
          </DetailedDescription>
        </Void>

        <Double Name="feature angle" Label="Feature Angle"
            Units="degrees" Optional="true" IsEnabledByDefault="false">
          <BriefDescription>The feature angle for sharp edge identification.</BriefDescription>
          <DefaultValue>45.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Max Inclusive="true">180.</Max>
          </RangeInfo>
        </Double>

        <Double Name="edge angle" Label="Edge Angle" Units="degrees">
          <BriefDescription>
            The edge angle to control smoothing along edges (either interior or boundary).
          </BriefDescription>
          <DefaultValue>15.</DefaultValue>
          <RangeInfo>
            <Min Inclusive="true">0.</Min>
            <Max Inclusive="true">180.</Max>
          </RangeInfo>
        </Double>

        <Void Name="boundary smoothing" Label="Smooth Boundaries"
              Optional="true" IsEnabledByDefault="true">
          <BriefDescription>
            Turn on/off the smoothing of points on the boundary of the mesh.
          </BriefDescription>
        </Void>

        <Void Name="non-manifold smoothing" Label="Smooth Non-manifold Points"
              Optional="true" IsEnabledByDefault="false">
          <BriefDescription>
            Turn on/off the smoothing of non-manifold points.
          </BriefDescription>
        </Void>

      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(smooth surface)" BaseType="result"/>
  </Definitions>
</SMTK_AttributeResource>
