// Copyright (c) Kitware, Inc. All rights reserved. See license.md for details.
#ifndef smtk_session_aeva_Resource_h
#define smtk_session_aeva_Resource_h
/*!\file */

#include "smtk/model/Resource.h"
#include "smtk/resource/DerivedFrom.h"
#include "smtk/resource/Manager.h"
#include "smtk/session/aeva/Session.h"

#include "smtk/session/aeva/Exports.h"

namespace smtk
{
namespace session
{
namespace aeva
{

/// A resource for anatomical annotation.
class SMTKAEVASESSION_EXPORT Resource
  : public smtk::resource::DerivedFrom<Resource, smtk::model::Resource>
{
public:
  smtkTypeMacro(smtk::session::aeva::Resource);
  smtkSuperclassMacro(smtk::resource::DerivedFrom<Resource, smtk::model::Resource>);
  smtkSharedPtrCreateMacro(smtk::resource::Resource);

  virtual ~Resource();

  /// The resource holds "session" data used to map underlying model data into SMTK.
  const Session::Ptr& session() const { return m_session; }
  void setSession(const Session::Ptr&);

  /// We override find() in order to return primitive-selection data not explicitly
  /// held by the resource (and thus not serialized).
  smtk::resource::ComponentPtr find(const smtk::common::UUID& compId) const override;

protected:
  Resource(const smtk::common::UUID&, smtk::resource::Manager::Ptr const& manager = nullptr);
  Resource(smtk::resource::Manager::Ptr const& manager = nullptr);

  Session::Ptr m_session;
};

} // namespace aeva
} // namespace session
} // namespace smtk
#endif // smtk_session_aeva_Resource_h
