cmake_minimum_required(VERSION 3.12)

# In order to use a new superbuild, please copy the item from the date stamped
# directory into the `keep` directory. This ensure that new versions will not
# be removed by the uploader script and preserve them for debugging in the
# future.

set(data_host "https://data.kitware.com")

# Determine the tarball to download.
if ("$ENV{CMAKE_CONFIGURATION}" MATCHES "vs2019")
  # 20200806
  set(file_item "5f2cb9ea9014a6d84e607a2e")
  set(file_hash "b9f64a511bad533d758cc50d929dbbbe7924b2cddaff883e0698f79c4c6abf97f7a350c6bd62b5a6866f384a446e5730166ef7695a87df12d9038112876837e9")
elseif ("$ENV{CMAKE_CONFIGURATION}" MATCHES "macos")
  # 20200718
  set(file_item "5f131bf09014a6d84e28ce87")
  set(file_hash "e3c5404fac448d4a27013f4f5a5ca7363450aa39c192c9e97148e266771b563f1057c442dc29d246fffdfba10a13fe28378beb2c73e2c3c735171108b5496833")
else ()
  message(FATAL_ERROR
    "Unknown build to use for the superbuild")
endif ()

# Ensure we have a hash to verify.
if (NOT DEFINED file_item OR NOT DEFINED file_hash)
  message(FATAL_ERROR
    "Unknown file and hash for the superbuild")
endif ()

# Download the file.
file(DOWNLOAD
  "${data_host}/api/v1/item/${file_item}/download"
  ".gitlab/superbuild.tar.gz"
  STATUS download_status
  EXPECTED_HASH "SHA512=${file_hash}")

# Check the download status.
list(GET download_status 0 res)
if (res)
  list(GET download_status 1 err)
  message(FATAL_ERROR
    "Failed to download superbuild.tar.gz: ${err}")
endif ()

# Extract the file.
execute_process(
  COMMAND
    "${CMAKE_COMMAND}"
    -E tar
    xf ".gitlab/superbuild.tar.gz"
  RESULT_VARIABLE res
  ERROR_VARIABLE err
  ERROR_STRIP_TRAILING_WHITESPACE)
if (res)
  message(FATAL_ERROR
    "Failed to extract superbuild.tar.gz: ${err}")
endif ()
